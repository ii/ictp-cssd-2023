2023 ICTP Collaborative programming school
==========================================

In this repository, we'll include all relevant material, it will be updated frequently during the school.

Material
---------

Day 1 - Monday
..............

* `Python overview <day_01/intro.pdf>`_
* `Git Introduction (solo) <day_01/git_intro.pdf>`_
* `Project Euler examples 1,2,3 (for basic language features), 14, 17 (dict), 57, 79 (file input), 102 (2D points) <http://projecteuler.net/archives>`_

Day 2 - Tuesday
...............
* `Clean Code <day_02/Clean_code.pdf>`_ and `Docstrings <day_02/Docstrings.pdf>`_
* `Object oriented design <day_02/OO-Design.pdf>`_
  - (`Trains example data to play with <day_02/trains>`_)
* Visualisation intro
* Titanic example

Day 3 - Wednesday
.................
* `Numpy activity <https://github.com/Sera91/SMR3894-Python-Tutorials>`_
* `picking colors <day_03/Color.pdf>`_

Day 4 - Thursday
................
* `The Pocket Guide to Debugging <https://wizardzines.com/zines/debugging-guide/>`_
* `Git branches <day_04/git_branches.pdf>`_

Day 5 - Friday
..............

* `Optimization <day_05/Optimization.pdf>`_
* `Git for groups <day_05/git-for-groups.pdf>`_
* profiling
* `interactive visualisation <day_05/Vis_interact.pdf>`_ (`slider <day_05/slider_demo.py>`_ and `interactive altair <day_05/interact_altair.py>`_)

Day 6 - Monday
..............

* `Unit tests <day_06/Unit_testing_slides.pdf>`_ (`examples <day_06/UT/test_standalone.py>`_ and `exercises <day_06/UT/problems>`_)
* Continuous integration (at the end of day_05 `git-for-groups.pdf <day_05/git-for-groups.pdf>`_ slides)
* `Group projects <https://ictp.grelli.org/cssd2023/index.html>`_


Day 7 - Tuesday
...............

* `packaging <day_07/pypackaging.pdf>`_ (`example toml file <day_07/pyproject.toml>`_)
* `Makefile <day_07/Makefile.pdf>`_ (`examples <day_07/Makefile_examples>`_ and `exercises <day_07/Makefile_exercises>`_)
* `The Dream Book <https://git.smr3696.ictp.it/acorbe/the-dream-book>`_ project


Day 8 - Wednesday
.................

* `Docker <day_08/docker.pdf>`_

..
   Discord
   -------

   We use Discord for sharing short links etc. during the sessions, and for coordinating 
   the group work in the second week. You can also use it to chat with the other participants.
